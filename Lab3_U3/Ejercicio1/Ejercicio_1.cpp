#include <iostream>
#include <fstream>
#include <cstring>
#include <time.h>

using namespace std;


// Función que determina cuanto tiempo se demora en ejecutar el programa.
float calcularTiempo(float tiempoInicial, float tiempoFinal){
	float ejecucion = (tiempoFinal - tiempoInicial)/CLOCKS_PER_SEC;
	return ejecucion;
}


// Función para contar los caracteres del archivo de texto.
int contarCaracteres(ifstream &archivo){
	int cant_caracteres = 0;
	char linea[1000];
	
	// Ciclo que recorre el archivo de texto.
	while(!archivo.eof()){
		// Se obtiene la linea del archivo.
		archivo.getline(linea, 1000);
		// Se saca la cantidad de caracteres de la linea evaluada antes y se va sumando con cada linea.
		cant_caracteres = cant_caracteres + strlen(linea);
	}
	// Se imprime la cantidad de caracteres del archivo
	cout << "La cantidad de caracteres es: " << cant_caracteres << endl;
	return cant_caracteres;
}


// Función que cuenta las palabras del archivo.
int contarPalabras(ifstream &archivo){
	int cant_palabras = 0;
	char linea[1000];
	
	// Ciclo que recorre el archivo.
	while(!archivo.eof()){
		// Se obtiene la linea del archico.
		archivo.getline(linea, 1000);
		
		// Ciclo que recorre y evalua la linea del archivo.
		for(int i=0;i<strlen(linea);i++){
			// Se ve cuando hay espacios para determinar cuantas palabras hay ya que estas se separan 
			// por los espacios y tambien cuando hay un salto de linea ya que no contaria la primera palabra de la siguiente linea.
			if((linea[i]==' ' || linea[i+1]== '\0')){
				// Se van sumando las cantidad de palabras de las lineas.
				cant_palabras++;
			}
		}
	}
	// Se imprime la cantidad de palabras en el archivo evaluado.
	cout << "La cantidad de palabras es: " << cant_palabras << endl;
	return cant_palabras;
}


// Función que determina la cantidad de lineas del archivo.
int contarLineas(ifstream &archivo){
	int cant_lineas = 0;
	char linea[1000];
	
	// Ciclo que recorre el archivo.
	while(!archivo.eof()){
		// Se obtiene la la linea del archivo.
		archivo.getline(linea, 1000);
		// Se van sumando las lineas.
		cant_lineas++;
	}
	
	// Se imprime la cantidad de lineas del archivo evaluado.
	cout << "La cantidad de lineas es: " << cant_lineas << endl;
	return cant_lineas;
}


// Función principal.
int main(int argc, char* argv[]){
	int total_lineas = 0;
	int total_palabras = 0;
	int total_caracteres = 0;
	float tiempoInicial, tiempoFinal;
	
	ifstream archivo;
	
	// Incico de contea del tiempo.
	tiempoInicial = clock();
	
	// Ciclo que evalua los archivos ingresados.
	for(int i=1; i<argc; i++){
		if(argc <= 1){
			cout << "Ingrese archivos " << endl;
		}
		
		// Se imprime el nombre del archivo.
		cout << endl;
		cout << "El nombre del archivo es: " << argv[i] << endl;
		
		// Se manda el archivo a la función contarLineas para determinar la cantidad de lineas que hay en el archivo.
		archivo.open(argv[i]);
		// Se suman las lineas de todos los archivos ingresados.
		total_lineas = total_lineas + contarLineas(archivo);
		archivo.close();
		
		// Se manda el archivo a la función contarPalabras para determinar la cantidad de palabras que hay en el archivo.
		archivo.open(argv[i]);
		// Se suman las palabras de todos los archivos ingresados.
		total_palabras = total_palabras + contarPalabras(archivo);
		archivo.close();
		
		// Se manda el archivo a la función contarCaracteres para determinar la cantidad de caracteres que hay en el archivo.
		archivo.open(argv[i]);
		// Se suman los caracteres de todos los archivos ingresados.
		total_caracteres = total_caracteres + contarCaracteres(archivo);
		archivo.close();	
	}
	
	// Termina el conteo del tiempo.
	tiempoFinal = clock();
	
	// Se imprimen los totales de los datos pedidos.
	cout << endl;
	cout << "Entre los archivos revisados: " << endl;
	cout << "La cantidad total de lineas es: " << total_lineas << endl;
	cout << "La cantidad total de palabras es: " << total_palabras << endl;
	cout << "La cantidad total de caracteres es: " << total_caracteres << endl;
	cout << endl;
	cout << "El tiempo de ejecución es: " << calcularTiempo(tiempoInicial, tiempoFinal) << "segundos" << endl;
	
	return 0;
}

