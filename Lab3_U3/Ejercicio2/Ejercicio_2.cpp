#include <iostream>
#include <fstream>
#include <cstring>
#include <pthread.h>
#include <time.h>

using namespace std;

// Estructura del ejercicio 2
struct ejer2_structura{
	ifstream archivo;
	int cant_lineas;
	int cant_palabras;
	int cant_caracteres;
};


// Función que determina cuanto tiempo se demora en ejecutar el programa.
float calcularTiempo(float tiempoInicial, float tiempoFinal){
	float ejecucion = (tiempoFinal - tiempoInicial)/CLOCKS_PER_SEC;
	return ejecucion;
}


// Función para contar los caracteres del archivo de texto.
void *contarCaracteres(void *param){
	// Se recibe el archivo.
	ejer2_structura *recibir;
	recibir = (ejer2_structura *)param;
	
	int cant_caracteres = 0;
	char linea[100];

	// Ciclo que recorre el archivo de texto.
	while(!recibir->archivo.eof()){
		// Se obtiene la linea del archivo.
		recibir -> archivo.getline(linea, 1000);
		// Se saca la cantidad de caracteres de la linea evaluada antes y se va sumando con cada linea.
		cant_caracteres = cant_caracteres + strlen(linea);
	}

	// Se imprime la cantidad de caracteres del archivo.
	recibir -> cant_caracteres = cant_caracteres;
	cout << "La cantidad de caracteres es: " << cant_caracteres << endl;
	pthread_exit(0);
}


// Función que cuenta las palabras del archivo.
void *contarPalabras(void *param){
	// Se recibe el archivo.
	ejer2_structura *recibir;
	recibir = (ejer2_structura *)param;

	int cant_palabras = 0;
	char linea[1000];
	
	// Ciclo que recorre el archivo.
	while(!recibir->archivo.eof()){
		// Se obtiene la linea del archico.
		recibir -> archivo.getline(linea, 1000);
		
		// Ciclo que recorre y evalua la linea del archivo.
		for(int i=0;i<strlen(linea);i++){
			// Se ve cuando hay espacios para determinar cuantas palabras hay ya que estas se separan 
			// por los espacios y tambien cuando hay un salto de linea ya que no contaria la primera palabra de la siguiente linea.
			if((linea[i]==' ' || linea[i+1]== '\0')){
				// Se van sumando las cantidad de palabras de las lineas.
				cant_palabras++;
			}
		}
	}


	// Se imprime la cantidad de palabras en el archivo evaluado.
	recibir -> cant_palabras = cant_palabras;
	cout << "La cantidad de palabras es: " << cant_palabras << endl;
	pthread_exit(0);
}

// Función que determina la cantidad de lineas del archivo.
void *contarLineas(void *param){
	// Se recibe el archivo 
	ejer2_structura *recibir;
	recibir = (ejer2_structura *)param;
	
	int cant_lineas = 0;
	char linea[1000];

	// Ciclo que recorre el archivo.
	while(!recibir->archivo.eof()){
		// Se obtiene la la linea del archivo.
		recibir -> archivo.getline(linea, 1000);
		// Se van sumando las lineas.
		cant_lineas++;
	}

	// Se imprime la cantidad de lineas del archivo evaluado.
	recibir -> cant_lineas = cant_lineas;
	cout << "La cantidad de lineas es: " << cant_lineas << endl;
	pthread_exit(0);

}


// Función principal.
int main(int argc, char* argv[]){
	
	// Se crea la estructura.
	pthread_t threads[argc*3-1];
	ejer2_structura contenido;

	int cant_total_lineas = 0;
	int cant_total_palabras = 0;
	int cant_total_caracteres = 0;
	float tiempoInicial, tiempoFinal;

	// Incico de contea del tiempo.
	tiempoInicial = clock();

	// Ciclo que evalua los archivos ingresados.
	for(int i=1;i<argc;i++){
		
		if(argc <= 1){
			cout << "sin archivos" << endl;
		}
		
		// Se imprime el nombre del archivo.
		cout << endl;
		cout << "Nombre del archivo: " << argv[i] << endl;

		// Se manda el archivo a la función contarLineas para determinar la cantidad de lineas que hay en el archivo.
		contenido.archivo.open(argv[i]);
		pthread_create(&threads[i-1], NULL, contarLineas, (void *)&contenido);
		pthread_join(threads[i-1], NULL);
		// Se suman las lineas de todos los archivos ingresados.
		cant_total_lineas = cant_total_lineas + contenido.cant_lineas;
		contenido.archivo.close();
		

		// Se manda el archivo a la función contarPalabras para determinar la cantidad de palabras que hay en el archivo.
		contenido.archivo.open(argv[i]);
		pthread_create(&threads[i-1], NULL, contarPalabras, (void *)&contenido);
		pthread_join(threads[i-1], NULL);
		// Se suman las palabras de todos los archivos ingresados.
		cant_total_palabras = cant_total_palabras + contenido.cant_palabras;
		contenido.archivo.close();
		

		// Se manda el archivo a la función contarCaracteres para determinar la cantidad de caracteres que hay en el archivo.
		contenido.archivo.open(argv[i]);
		pthread_create(&threads[i-1], NULL, contarCaracteres, (void *)&contenido);
		pthread_join(threads[i-1], NULL);
		// Se suman los caracteres de todos los archivos ingresados.
		cant_total_caracteres = cant_total_caracteres + contenido.cant_caracteres;
		contenido.archivo.close();
	}
	
	// Termina el conteo del tiempo.
	tiempoFinal = clock();

	// Se imprimen los totales de los datos pedidos.
	cout << endl;
	cout << "Entre los archivos revisados: " << endl;
	cout << "La cantidad total de lineas es: " << cant_total_lineas << endl;
	cout << "La cantidad total de palabras es: " << cant_total_palabras << endl;
	cout << "La cantidad total de caracteres es: " << cant_total_caracteres << endl;
	cout << endl;
	cout << "El tiempo de ejecución es: " << calcularTiempo(tiempoInicial, tiempoFinal) << "segundos" << endl;
	
	return 0;
}
