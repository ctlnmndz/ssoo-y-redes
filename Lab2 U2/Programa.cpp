#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
#include <libgen.h>

using namespace std;

// Se crea la clase del programa.
class Fork {
	// Variables privadas.
	private:

		pid_t pid;
		int segundos;
		char* link;

	// Variables públicas.
	public:

		Fork (int seg, char* descarga) {
			link = descarga;
			segundos = seg;
			
			// Métodos.
			CrearProceso();
			programa();
		}


		// Método que crea fork.
		void CrearProceso() {
		// Se crea el proceso hijo con fork.
			pid = fork();
		}

		// Método que extrae y reproduce el audio del video.
		void programa() {
			
			// Si la variable "pid" es menor a 0 no es valido y no se crea nada.
			if (pid < 0) {
				cout << "No es posible crear el proceso, F :(";
				cout << endl;
			}
			
			// Cuando "pid" es igual a 0 se crea el proceso y se obtiene el audio del link inducido.
			else if (pid == 0) {
				cout << "Extrayendo audio, espere unos segundos <3";
				cout << endl;
				
				// Se utilizan los sigueientes comandos para extraer el audio del video con youtube-dl.
				// -x para descargar el video, --audio-format mp3 para descargar el audio en formato mp3.
				execlp("youtube-dl", "youtube-dl", "-x", "--audio-format", "mp3", link, NULL);
				sleep(segundos);
			}
			// Proceso padre.
			else{
 
				wait (NULL);
				cout << "La canción se esta reproduciendo, chiviri";
				cout << endl;

				// Se utiliza el programa sox para reproducir la musica descargada.
				execlp("sox", "play", "*.mp3", NULL);
			}
		}
};

// Función principal.
int main(int argc, char *argv[]){

  // se le pide el link url al usuario.
	char* link = argv[1];

  // Se llama la clase fork para extraer el audio.
	Fork fork(1, link);

  return 0;
}
